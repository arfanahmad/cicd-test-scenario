'''This is some trivial code.
'''


def my_print(to_be_printed):
    """"a custom print function"""
    print(to_be_printed)

if __name__ == "__main__":
    my_print("Hello, world!")
